<?php

use Farmer\Animal;
use Farmer\Herd\Herd;

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Pig, 1);
$herd->reproduce(new Animal\Rabbit, new Animal\Pig);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Pig, 1);
$herd->reproduce(new Animal\Sheep, new Animal\Pig);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 5);
$herd->addAnimals(new Animal\Cow, 1);
$herd->reproduce(new Animal\Sheep, new Animal\Pig);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 4);
$herd->addAnimals(new Animal\Sheep, 2);
$herd->addAnimals(new Animal\Horse, 1);
$herd->reproduce(new Animal\Pig, new Animal\Pig);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Sheep, 1);
$herd->addAnimals(new Animal\Pig, 2);
$herd->exchange(new Animal\Rabbit, new Animal\Sheep);
$herd->exchange(new Animal\Sheep, new Animal\Pig);
$herd->exchange(new Animal\Pig, new Animal\Cow);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Horse, 1);
$herd->exchange(new Animal\Horse, new Animal\Cow);
$herd->exchange(new Animal\Cow, new Animal\Pig);
$herd->exchange(new Animal\Pig, new Animal\Sheep);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Sheep, 6);
$herd->addAnimals(new Animal\Pig, 6);
$herd->addAnimals(new Animal\Cow, 6);
$herd->addAnimals(new Animal\Horse, 6);
$herd->addAnimals(new Animal\Dog, 1);
$herd->attack(new Animal\Wolf);
$herd->attack(new Animal\Fox);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Sheep, 6);
$herd->addAnimals(new Animal\Pig, 6);
$herd->addAnimals(new Animal\Cow, 6);
$herd->addAnimals(new Animal\Horse, 6);
$herd->addAnimals(new Animal\Dog, 1);
$herd->attack(new Animal\Fox);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Sheep, 6);
$herd->addAnimals(new Animal\Pig, 6);
$herd->addAnimals(new Animal\Cow, 6);
$herd->addAnimals(new Animal\Horse, 6);
$herd->addAnimals(new Animal\BigDog, 1);
$herd->attack(new Animal\Wolf);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Sheep, 6);
$herd->addAnimals(new Animal\Pig, 6);
$herd->addAnimals(new Animal\Cow, 6);
$herd->addAnimals(new Animal\Horse, 6);
$herd->attack(new Animal\Fox);
dump($herd);

$herd = new Herd();
$herd->addAnimals(new Animal\Rabbit, 6);
$herd->addAnimals(new Animal\Sheep, 6);
$herd->addAnimals(new Animal\Pig, 6);
$herd->addAnimals(new Animal\Cow, 6);
$herd->addAnimals(new Animal\Horse, 6);
$herd->addAnimals(new Animal\Dog, 1);
$herd->attack(new Animal\Wolf);
dump($herd);

function dump($herd)
{
	echo "<pre>";
	var_dump($herd->getAnimals());
	echo "</pre>";
}