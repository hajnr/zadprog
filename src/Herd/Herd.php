<?php namespace Farmer\Herd;

//use Farmer\Contracts\AnimalInterface as Animal;
use Farmer\Animal\{Animal, Dog, BigDog, Fox, Wolf, Rabbit, Horse};
use Farmer\Controller\ExchangeRatio as Ratio;

class Herd {
	
	private $animals = [];

	public function addAnimals(Animal $animal, int $quantity) 
	{
		$key = get_class($animal);
		if($this->herdHasAnimal($animal, $key))
		{ 
			$this->animals[$key] += $quantity;
		}
		else 
		{
			$this->animals[$key] = $quantity;
		} 
	}

	public function reproduce(Animal $animal1, Animal $animal2) 
	{
		foreach(func_get_args() as $animal)
		{
			if($this->herdHasAnimal($animal))
			{
				$pairs = $this->getPairs($animal);
				$this->addAnimals($animal, $pairs);
			}
		}

		if($animal1 == $animal2) 
		{
			$this->addAnimals($animal1, 1);
		}
	}
	
	public function exchange(Animal $animal1, Animal $animal2) 
	{
		$ratio = (new Ratio($key1 = get_class($animal1), $key2 = get_class($animal2)))->getRatio();

		if($this->herdHasAnimal($animal1, $key1))
		{
			$this->removeAnimals($animal1, $ratio['del']);
			$this->addAnimals($animal2, $ratio['add']);
		}
	}
	
	public function attack(Animal $animal) 
	{
		if($this->isFox($animal))
		{
			if($this->herdHasAnimal(new Dog))
			{
				$this->removeAnimals(new Dog, 1);
			}
			else
			{
				$this->removeAnimals(new Rabbit);
			}
		}

		if($this->isWolf($animal))
		{
			if($this->herdHasAnimal(new BigDog))
			{
				$this->removeAnimals(new BigDog, 1);
			}
			else
			{
				foreach($this->getAnimals() as $key => $herdAnimal)
				{
					if(($key == get_class(new Horse)) || ($key == get_class(new Dog)))
					{
						continue;
					}
					unset($this->animals[$key]);
				}
			}
		}

	}
	
	public function getAnimals() 
	{
		return $this->animals;
	}

	protected function herdHasAnimal(Animal $animal, String $key = null)
	{
		if(is_null($key))
		{
			$key = get_class($animal);
		}

		if(array_key_exists($key, $this->animals)) 
		{
			return true;
		}
		return false;
	}

	protected function getPairs(Animal $animal)
	{
		return ( floor(($this->animals[get_class($animal)] + 1) / 2) );
	}

	protected function removeAnimals(Animal $animal, int $quantity = null)
	{
		$key = get_class($animal);

		if(is_null($quantity))
		{
			unset($this->animals[$key]);
		}
		else
		{
			$this->animals[$key] -= $quantity;
			if($this->animals[$key] <= 0)
			{
				unset($this->animals[$key]);
			}
		}
	}

	protected function isFox(Animal $animal)
	{
		if($animal == (new Fox))
		{
			return true;
		}
		return false;
	}

	protected function isWolf(Animal $animal)
	{
		if($animal == (new Wolf))
		{
			return true;
		}
		return false;
	}
}