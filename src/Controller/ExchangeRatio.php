<?php namespace Farmer\Controller;

class ExchangeRatio {
	
	const SHEEP = 'Farmer\Animal\Sheep';
	const RABBIT = 'Farmer\Animal\Rabbit';
	const PIG = 'Farmer\Animal\Pig';
	const COW = 'Farmer\Animal\Cow';
	const HORSE = 'Farmer\Animal\Horse';
	const DOG = 'Farmer\Animal\Dog';
	const BIGDOG = 'Farmer\Animal\BigDog';

	protected $ratio = [];

	public function __construct(String $animal1, String $animal2)
	{
		if( strcmp(self::SHEEP, $animal1) == 0 && strcmp(self::RABBIT, $animal2) == 0 )
		{
			$this->setRatio(1, 6);
		} 
		elseif( strcmp(self::RABBIT, $animal1) == 0 && strcmp(self::SHEEP, $animal2) == 0 )
		{
			$this->setRatio(6, 1) == 0;
		}
		elseif( strcmp(self::PIG, $animal1) == 0 && strcmp(self::SHEEP, $animal2) == 0 )
		{
			$this->setRatio(1, 2);
		}
		elseif( strcmp(self::SHEEP, $animal1) == 0 && strcmp(self::PIG, $animal2) == 0 )
		{
			$this->setRatio(2, 1) == 0;
		}
		elseif( strcmp(self::COW, $animal1) == 0 && strcmp(self::PIG, $animal2) == 0 )
		{
			$this->setRatio(1, 3);
		}
		elseif( strcmp(self::PIG, $animal1) == 0 && strcmp(self::COW, $animal2) == 0 )
		{
			$this->setRatio(3, 1) == 0;
		}
		elseif( strcmp(self::HORSE, $animal1) == 0 && strcmp(self::COW, $animal2) == 0 )
		{
			$this->setRatio(1, 2);
		}
		elseif( strcmp(self::COW, $animal1) == 0 && strcmp(self::HORSE, $animal2) == 0 )
		{
			$this->setRatio(2, 1) == 0;
		}
		elseif( strcmp(self::DOG, $animal1) == 0 && strcmp(self::SHEEP, $animal2) == 0 )
		{
			$this->setRatio(1, 1) == 0;
		}
		elseif( strcmp(self::SHEEP, $animal1) == 0 && strcmp(self::DOG, $animal2) == 0 )
		{
			$this->setRatio(1, 1) == 0;
		}
		elseif( strcmp(self::BIGDOG, $animal1) == 0 && strcmp(self::SHEEP, $animal2) == 0 )
		{
			$this->setRatio(1, 1) == 0;
		}
		elseif( strcmp(self::SHEEP, $animal1) == 0 && strcmp(self::BIGDOG, $animal2) == 0 )
		{
			$this->setRatio(1, 1) == 0;
		}
		else 
		{
			$this->setRatio(0, 0);
		}
	}

	public function getRatio()
	{
		return $this->ratio;
	}

	protected function setRatio($a, $b)
	{
		$this->ratio = ['del' => $a, 'add' => $b];
	}
}